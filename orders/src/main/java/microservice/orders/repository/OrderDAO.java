package microservice.orders.repository;

import microservice.orders.model.OrderDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class OrderDAO {

    private static final String INSERT_CUSTOMER_ORDER =
            "INSESRT INTO order (" +
                    " ornumber," +
                    " ordername," +
                    " ordertype," +
                    " orderstorage," +
                    " ordercolor) " +
                    "VALUES (" +
                    " :ordernumber," +
                    " :ordername," +
                    " :ordertype," +
                    " :orderstorage," +
                    " :ordercolor)";

    private static final String RETRIEVE_SQL =
            "SELECT * FROM customerorder " +
                    "WHERE coordernumber = :orderNumber";

    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public OrderDAO(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void create(OrderDetails orderDetails) {
        jdbcTemplate.update(INSERT_CUSTOMER_ORDER, buildQueryParametersForCreate(orderDetails));
    }

    private MapSqlParameterSource buildQueryParametersForCreate(OrderDetails customerOrderDetails) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();

        parameterSource.addValue("orderno", customerOrderDetails.getOrderNumber());
        parameterSource.addValue("ordername", customerOrderDetails.getOrderName());
        parameterSource.addValue("ordertype", customerOrderDetails.getOrderType());
        parameterSource.addValue("orderstorage", customerOrderDetails.getOrderStorage());
        parameterSource.addValue("ordercolor", customerOrderDetails.getOrderColor());

        return parameterSource;
    }

    private Map<String, Object> buildParamForRetrieve(String orderNumber) {
        Map<String, Object> params = buildOrderNumberParam(orderNumber);

        return params;
    }

    private Map<String, Object> buildOrderNumberParam(String orderNumber) {
        Map<String, Object> params = new HashMap<>();
        params.put("orderNumber", orderNumber.toString());

        return params;
    }

}

