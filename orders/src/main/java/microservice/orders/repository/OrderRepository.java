package microservice.orders.repository;

import microservice.orders.model.OrderDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class OrderRepository {
    private final OrderDAO orderDAO;

    @Autowired
    public OrderRepository(OrderDAO orderDAO) {
        this.orderDAO = orderDAO;
    }

    @Transactional
    public void create(OrderDetails orderDetails) {
        orderDAO.create(orderDetails);
    }

}

