package microservice.orders.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderDetails {
    private final String orderNumber;
    private final String orderName;
    private final String orderType;
    private final Number orderStorage;
    private final String orderColor;

    public OrderDetails(
            @JsonProperty("orderNumber") String orderNumber,
            @JsonProperty("orderName") String orderName,
            @JsonProperty("orderType") String orderType,
            @JsonProperty("orderStorage") Number orderStorage,
            @JsonProperty("orderColor") String orderColor
    ) {
        this.orderNumber = orderNumber;
        this.orderName = orderName;
        this.orderType = orderType;
        this.orderStorage = orderStorage;
        this.orderColor = orderColor;
    }
}


