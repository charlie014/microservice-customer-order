package microservice.orders.controller;

import microservice.orders.model.OrderDetails;
import microservice.orders.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@Api(tags = "Customer Order")
public class OrderController {

    private final OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @ApiOperation(value = "Create Customer Order", notes = "Create a new customer order", produces = APPLICATION_JSON_VALUE)
    @ApiResponses({
            @ApiResponse(code = 201, message = "Create customer order successfully")
    })
    @RequestMapping(method = RequestMethod.POST, value = "/customer-order", produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public String createCustomerOrder(
            @ApiParam(required = true)
            @RequestBody OrderDetails orderDetails) {
        return orderService.create(orderDetails);
    }
}

