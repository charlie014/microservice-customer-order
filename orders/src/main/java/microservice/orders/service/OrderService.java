package microservice.orders.service;

import microservice.orders.model.OrderDetails;
import microservice.orders.repository.OrderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class OrderService {

    private static Logger logger = LoggerFactory.getLogger(OrderService.class);
    private final OrderRepository orderRepository;

    @Autowired
    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Transactional
    public String create(OrderDetails orderDetails) {
        save(orderDetails);

        return orderDetails.getOrderNumber();
    }

    private void save(OrderDetails orderDetails) {
        orderRepository.create(orderDetails);
    }
}

