package mircoservice.order.model;

public class CustomerOrderConfirmation {

    public Integer orderNumber;
    public String orderName;
    public String orderType;
    public Integer orderStorage;
    public String orderColor;
}
