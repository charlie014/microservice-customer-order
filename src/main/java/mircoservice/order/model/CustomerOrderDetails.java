package mircoservice.order.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class CustomerOrderDetails {
    private final Integer orderNumber;
    private final String orderName;
    private final String orderType;
    private final Number orderStorage;
    private final String orderColor;

    public CustomerOrderDetails(
            @JsonProperty("orderNumber") Integer orderNumber,
            @JsonProperty("orderName") String orderName,
            @JsonProperty("orderType") String orderType,
            @JsonProperty("orderStorage") Number orderStorage,
            @JsonProperty("orderColor") String orderColor
    ) {
        this.orderNumber = orderNumber;
        this.orderName = orderName;
        this.orderType = orderType;
        this.orderStorage = orderStorage;
        this.orderColor = orderColor;
    }
}

