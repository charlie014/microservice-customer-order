package mircoservice.order.repository;

import mircoservice.order.model.CustomerOrderConfirmation;
import mircoservice.order.model.CustomerOrderDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class CustomerOrderRepository {
    private final CustomerOrderDAO customerOrderDAO;

    @Autowired
    public CustomerOrderRepository(CustomerOrderDAO customerOrderDAO) {
        this.customerOrderDAO = customerOrderDAO;
    }

    @Transactional
    public void create(CustomerOrderDetails customerOrderDetails) {
        customerOrderDAO.create(customerOrderDetails);
    }

    @Transactional
    public CustomerOrderConfirmation retrieve(Integer orderNumber) {

        return customerOrderDAO.retrieve(orderNumber);
    }
}
