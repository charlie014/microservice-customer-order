package mircoservice.order.repository;

import mircoservice.order.exception.CustomerOrderNotFoundException;
import mircoservice.order.model.CustomerOrderConfirmation;
import mircoservice.order.model.CustomerOrderDetails;
import mircoservice.order.utils.CustomerOrderRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class CustomerOrderDAO {

    private static final String INSERT_CUSTOMER_ORDER =
            "INSERT INTO sys.order (" +
                    " orderNumber," +
                    " orderName," +
                    " orderType," +
                    " orderStorage," +
                    " orderColor) " +
                    "VALUES (" +
                    " :orderNumber," +
                    " :orderName," +
                    " :orderType," +
                    " :orderStorage," +
                    " :orderColor)";

    private static final String RETRIEVE_SQL =
            "SELECT * FROM sys.order " +
                    "WHERE orderNumber = :orderNumber";

    private final NamedParameterJdbcTemplate jdbcTemplate;

    private final CustomerOrderRowMapper customerOrderRowMapper;

    @Autowired
    public CustomerOrderDAO(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.customerOrderRowMapper = new CustomerOrderRowMapper();
    }

    public Integer create(CustomerOrderDetails customerOrderDetails) {
        if(hasNowRowInserted(jdbcTemplate.update(INSERT_CUSTOMER_ORDER, buildQueryParametersForCreate(customerOrderDetails)))) {
            throw new CustomerOrderNotFoundException(customerOrderDetails.getOrderNumber());
        }
        return customerOrderDetails.getOrderNumber();
    }

    private MapSqlParameterSource buildQueryParametersForCreate(CustomerOrderDetails customerOrderDetails) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();

        parameterSource.addValue("orderNumber", customerOrderDetails.getOrderNumber());
        parameterSource.addValue("orderName", customerOrderDetails.getOrderName());
        parameterSource.addValue("orderType", customerOrderDetails.getOrderType());
        parameterSource.addValue("orderStorage", customerOrderDetails.getOrderStorage());
        parameterSource.addValue("orderColor", customerOrderDetails.getOrderColor());

        return parameterSource;
    }

    private boolean hasNowRowInserted(int rowsInserted) {
        return rowsInserted == 0;
    }

    public CustomerOrderConfirmation retrieve(Integer orderNumber) {
        validateOrderNumber(orderNumber);
        try {
            return jdbcTemplate.queryForObject(RETRIEVE_SQL, buildParamForRetrieve(orderNumber), customerOrderRowMapper);
        } catch (Exception e) {
            throw new CustomerOrderNotFoundException(orderNumber);
        }
    }

    private Map<String, Object> buildParamForRetrieve(Integer orderNumber) {

        return buildOrderNumberParam(orderNumber);
    }

    private Map<String, Object> buildOrderNumberParam(Integer orderNumber) {
        Map<String, Object> params = new HashMap<>();
        params.put("orderNumber", orderNumber.toString());

        return params;
    }

    private void validateOrderNumber(Integer orderNumber) {
        if (orderNumber == null) {
            throw new IllegalArgumentException("Order Number cannot be null");
        }
    }
}
