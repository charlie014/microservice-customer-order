package mircoservice.order.exception;

public class CustomerOrderNotFoundException extends RuntimeException {
    public CustomerOrderNotFoundException(Integer orderNumber) {
        super(String.format("Could not find any order number [%d]", orderNumber));
    }
}
