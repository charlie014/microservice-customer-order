package mircoservice.order.controller;

import io.swagger.annotations.*;
import mircoservice.order.model.CustomerOrderConfirmation;
import mircoservice.order.model.CustomerOrderDetails;
import mircoservice.order.service.CustomerOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@Api(tags = "Customer Order")
public class CustomerOrderController {

    private final CustomerOrderService customerOrderService;

    @Autowired
    public CustomerOrderController(CustomerOrderService customerOrderService) {
        this.customerOrderService = customerOrderService;
    }

    @ApiOperation(value = "Create Customer Order", notes = "Create a new customer order", produces = APPLICATION_JSON_VALUE)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Create customer order successfully")
    })
    @RequestMapping(method = RequestMethod.POST, value = "/customer-order", produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public Integer createCustomerOrder(
            @ApiParam(required = true)
            @RequestBody CustomerOrderDetails customerOrderDetails) {
        return customerOrderService.create(customerOrderDetails);
    }

    @ApiOperation(
            value = "Retrieve Customer Order",
            notes = "Retrieve an existing customer order",
            httpMethod = "GET",
            produces = APPLICATION_JSON_VALUE
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "Customer order details"),
            @ApiResponse(code = 404, message = "Customer order number not found"),
            @ApiResponse(code = 500, message = "Internal Server error")
    })
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.GET, value = "/customer-order/{orderNumber}")
    @ResponseBody
    public CustomerOrderConfirmation retrieveCustomerOrder(
            @ApiParam(name = "orderNumber", required = true, value = "The order number")
            @PathVariable("orderNumber") Integer orderNumber) {
        return customerOrderService.retrieve(orderNumber);
    }
}
