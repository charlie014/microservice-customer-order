package mircoservice.order.utils;

import mircoservice.order.model.CustomerOrderConfirmation;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class CustomerOrderRowMapper implements RowMapper<CustomerOrderConfirmation> {

    @Override
    public CustomerOrderConfirmation mapRow(ResultSet resultSet, int i) throws SQLException {
        CustomerOrderConfirmation customerOrderConfirmation = new CustomerOrderConfirmation();
        customerOrderConfirmation.orderNumber = resultSet.getInt("orderNumber");
        customerOrderConfirmation.orderName = resultSet.getString("orderName");
        customerOrderConfirmation.orderType = resultSet.getString("orderType");
        customerOrderConfirmation.orderStorage = resultSet.getInt("orderStorage");
        customerOrderConfirmation.orderColor = resultSet.getString("orderColor");

        return customerOrderConfirmation;
    }
}
