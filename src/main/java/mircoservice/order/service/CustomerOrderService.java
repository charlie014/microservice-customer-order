package mircoservice.order.service;

import mircoservice.order.model.CustomerOrderConfirmation;
import mircoservice.order.model.CustomerOrderDetails;
import mircoservice.order.repository.CustomerOrderRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CustomerOrderService {

    private static Logger logger = LoggerFactory.getLogger(CustomerOrderService.class);
    private final CustomerOrderRepository customerOrderRepository;

    @Autowired
    public CustomerOrderService(CustomerOrderRepository customerOrderRepository) {
        this.customerOrderRepository = customerOrderRepository;
    }

    @Transactional
    public Integer create(CustomerOrderDetails customerOrderDetails) {
        save(customerOrderDetails);

        return customerOrderDetails.getOrderNumber();
    }

    @Transactional
        public CustomerOrderConfirmation retrieve(Integer orderNumber) {
        validateCustomerOrderNumber(orderNumber);

        return customerOrderRepository.retrieve(orderNumber);
        }

    private void validateCustomerOrderNumber(Integer orderNumber) {
        if (orderNumber == null) {
            throw new IllegalArgumentException("Order Number cannot be null");
        }
    }

    private void save(CustomerOrderDetails customerOrderDetails) {
            customerOrderRepository.create(customerOrderDetails);
    }


}
