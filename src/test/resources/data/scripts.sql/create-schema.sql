CREATE TABLE IF NOT EXISTS `sys.order`
(
    orderNumber integer IDENTITY NOT NULL PRIMARY KEY,
    orderName varchar,
    orderType varchar,
    orderStorage integer,
    orderColor varchar
);