package mircoservice.order.controller;

import mircoservice.order.model.CustomerOrderDetails;
import mircoservice.order.service.CustomerOrderService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@ExtendWith(MockitoExtension.class)
public class CustomerOrderControllerTest {

    private MockMvc mockServer;

    private static final CustomerOrderDetails ORDER_DETAILS = new CustomerOrderDetails(
            1,
            "test-name",
            "test-type",
            128,
            "test-color");


    @Mock
    private CustomerOrderService mockCustomerOrderService;

    @BeforeEach
    public void setup() {
        mockServer = MockMvcBuilders.standaloneSetup(new CustomerOrderController(mockCustomerOrderService))
               .build();
    }

//    @Test
//    public void successfulCreateNewCustomerOrder() throws Exception{
//        mockServer.perform(requestToCreateNewCustomerOrder())
//                .andExpect(status().isOk());
//    }
//
//    @Test
//    public void shouldThrowExceptionWhenOrderIsNotCreated() throws Exception {
//        doThrow(new CustomerOrderNotFoundException(ORDER_DETAILS.getOrderNumber()))
//                .when(mockCustomerOrderService).create(ORDER_DETAILS);
//
//        mockServer.perform(requestToCreateNewCustomerOrder())
//                .andExpect(status().isNotFound())
//                .andExpect(failureCreateNeCustomerOrder("Could not find any order number [1]"));
//    }

    private MockHttpServletRequestBuilder requestToCreateNewCustomerOrder() {
        return MockMvcRequestBuilders.post("/customer-order", ORDER_DETAILS);
    }

    private ResultMatcher failureCreateNeCustomerOrder(String message) {
        return content().string(String.format("{\"url\":\"customer-order\", \"message\":\"%s\"}", message));
    }
}
