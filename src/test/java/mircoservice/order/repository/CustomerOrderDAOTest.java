package mircoservice.order.repository;

import mircoservice.order.model.CustomerOrderDetails;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ContextConfiguration
public class CustomerOrderDAOTest {

    private NamedParameterJdbcTemplate namedJdbcTemplate;
    private JdbcTemplate jdbcTemplate;
    private CustomerOrderDAO customerOrderDAO;
    private EmbeddedDatabase db;

    @Test
//    public void shouldCreateNewOrderRecord() {
//        customerOrderDAO.create(ORDER_DETAILS);
//        assertNewOrder(ORDER_DETAILS);
//    }

    private void assertNewOrder(CustomerOrderDetails customerOrderDetails) {
        Integer orderRecord = jdbcTemplate.queryForObject("SELECT COUNT(*) FROM sys.order WHERE orderNumber = " +
                customerOrderDetails.getOrderNumber().toString(), Integer.class);
        assertEquals(new Integer("1"), orderRecord);
    }

//    @Test
//    public void shouldThrowExceptionWhenOrderNumberIsNull() {
//        Exception thrown = Assertions.assertThrows(IllegalArgumentException.class, () ->
//                customerOrderDAO.retrieve(null)
//        );
//
//        assertTrue(thrown.getMessage().contains("Order Number cannot be null"));
//    }

//    @Test
//    public void retrievingOrderWithNoMatch() {
//        Exception thrown = Assertions.assertThrows(CustomerOrderNotFoundException.class, () ->
//                customerOrderDAO.retrieve(10)
//        );
//
//        assertTrue(thrown.getMessage().contains("Could not find any order number [10]"));
//    }

//    @Test
//    public void retrievingOrderSuccessfully() {
//        CustomerOrderConfirmation order = customerOrderDAO.retrieve(1);
//    }

    @BeforeEach
    public void setUp() {
        db = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .addScript("data/scripts.sql/create-schema.sql")
                .addScript("data/scripts.sql/sys-order-data.sql")
                .build();
        jdbcTemplate = new JdbcTemplate(db);
        customerOrderDAO = new CustomerOrderDAO(new NamedParameterJdbcTemplate(jdbcTemplate));
    }

    @AfterEach
    public void cleanUp() {
        db.shutdown();
    }
}
