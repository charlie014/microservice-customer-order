package mircoservice.order.service;

import mircoservice.order.exception.CustomerOrderNotFoundException;
import mircoservice.order.model.CustomerOrderDetails;
import mircoservice.order.repository.CustomerOrderDAO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.any;

@ExtendWith(MockitoExtension.class)
public class CustomerOrderServiceTest {

    private static final CustomerOrderDetails ORDER_DETAILS = new CustomerOrderDetails(
            1,
            "test-name",
            "test-type",
            128,
            "test-color");

    @Mock
    private CustomerOrderDAO customerOrderDAO;

    @Mock
    private CustomerOrderService customerOrderService;

//    @Test
//    public void shouldCreateNewCustomerOrderWhenGivenValidArguments() {
//        customerOrderService.create(ORDER_DETAILS);
//        verify(customerOrderDAO, times(1)).create(isA(CustomerOrderDetails.class));
//    }

    @Test
    public void shouldThrowExceptionWhenOrderNotCreated() {
        givenNoOrderCreated();
        Exception thrown = Assertions.assertThrows(CustomerOrderNotFoundException.class, () ->
                customerOrderDAO.create(ORDER_DETAILS)
        );

        assertTrue(thrown.getMessage().contains("Could not find any order number [1]"));
    }

    @Test
    public void retrievingCustomerOrderRequiredOrderNumber() {
        Exception thrown = Assertions.assertThrows(IllegalArgumentException.class, () ->
                customerOrderService.retrieve(null)
        );

        assertTrue(thrown.getMessage().contains("Order Number cannot be null"));
    }

    private void givenNoOrderCreated() {
        given(customerOrderDAO.create(any(CustomerOrderDetails.class)))
                .willThrow(new CustomerOrderNotFoundException(ORDER_DETAILS.getOrderNumber()));
    }

}
